const Admin = require("../models/admin.js");
const mongoose = require("mongoose");

const addAdmin = (req, res) => {
    Admin.create({ name: req.body.name, age: req.body.age }, function (err, doc) {
        if (err) return console.log(err);
        res.send(`Admin ${doc} has been created`)
    });
};

const getAdmins = async (req, res) => {
    Admin.find({ _id: req.params.id }, function (err, doc) {
        if (err) return console.log(err);
        res.send(`<h1>Admin name: ${doc}</h1>`);
    })
};

const deleteAdmin = async (req, res) => {
    Admin.findOneAndDelete({ _id: req.params.id }, function (err, doc) {
        if (err) return console.log(err);
        res.send(`Delete admin ${admin.name} ${admin.age}`);
    })
};

const putAdmin = async (req, res) => {
    Admin.findByIdAndUpdate({ _id: req.params.id }, { name: req.body.name, age: req.body.age }, function (err, user) {
        if (err) return console.log(err);
        res.send(`Update admin ${user}`);
    })
};

module.exports = { addAdmin, getAdmins, deleteAdmin, putAdmin }