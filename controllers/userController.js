const User = require("../models/user.js");
const mongoose = require("mongoose");

const addUser = (req, res) => {
    User.create({ name: req.body.name, age: req.body.age }, function (err, doc) {
        if (err) return console.log(err);
        res.send(`User ${doc} has been created`)
    });
};

const getUsers = (req, res) => {
    User.find({_id: req.params.id}, function(err, doc){
    if(err) return console.log(err);
        res.send(`<h1>User name: ${doc}</h1>`)
    })
};

const deleteUser = (req, res) => {
    User.findOneAndDelete({_id: req.params.id}, function(err, doc){
    if(err) return console.log(err);
        response.send(`Delete user ${doc}`);
    })
};

const putUser = (req, res) => {
    User.findByIdAndUpdate({_id: req.params.id}, {name: req.body.name, age: req.body.age}, function(err, user){
        if(err) return console.log(err);
        res.send(`Update user: ${user}`);
    })
};

module.exports = { addUser, getUsers, deleteUser, putUser }