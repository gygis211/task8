const express = require("express");
const userController = require("../controllers/userController");
const userRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
 
userRouter.post("/create", urlencodedParser, userController.addUser);
userRouter.get("/:id", urlencodedParser, userController.getUsers);
userRouter.delete("/:id", urlencodedParser, userController.deleteUser);
userRouter.put("/:id", urlencodedParser, userController.putUser);
 
module.exports = userRouter;