const express = require("express");
const adminController = require("../controllers/adminController");
const adminRouter = express.Router();
const bodyParser = require('body-parser');
 
adminRouter.post("/create", bodyParser.json(), adminController.addAdmin);
adminRouter.get("/:id", bodyParser.json(), adminController.getAdmins);
adminRouter.delete("/:id", bodyParser.json(), adminController.deleteAdmin);
adminRouter.put("/:id", bodyParser.json(), adminController.putAdmin);
 
module.exports = adminRouter;