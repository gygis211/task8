const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.urlencoded({ extended: true });
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017', { useUnifiedTopology: true, useNewUrlParser: true });

const userRouter = require('./routes/userRouter');
const adminRouter = require('./routes/adminRouter');

app.use("/user", urlencodedParser, userRouter);
app.use("/admin", urlencodedParser, adminRouter);


app.listen(3000);